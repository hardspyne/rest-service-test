
CREATE TABLE IF NOT EXISTS sale (
    id uuid PRIMARY KEY,
    created_time TIMESTAMPTZ,
    seller VARCHAR(9) NOT NULL,
    customer VARCHAR(9) NOT NULL
);


CREATE TABLE IF NOT EXISTS product (
    id uuid PRIMARY KEY,
    "name" VARCHAR(1000) NOT NULL,
    code VARCHAR(13) NOT NULL,
    created_time TIMESTAMPTZ,
    CONSTRAINT unique_code UNIQUE (code)

);


CREATE TABLE IF NOT EXISTS product_sales (
    sale_id uuid NOT NULL,
    product_id uuid NOT NULL,

    CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES product (id),
    CONSTRAINT fk_sale_id FOREIGN KEY (sale_id) REFERENCES sale (id)
);
