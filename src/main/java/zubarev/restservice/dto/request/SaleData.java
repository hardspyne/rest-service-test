package zubarev.restservice.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
public class SaleData {
    @NotBlank
    @Size(min = 9, max = 9)
    private String seller;

    @NotBlank
    @Size(min = 9, max = 9)
    private String customer;

    @NotEmpty
    @Valid
    List<ProductDto> products;
}
