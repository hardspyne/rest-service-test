package zubarev.restservice.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ProductDto {

    @NotBlank
    private String name;

    @NotBlank
    @Size(min = 13, max = 13)
    private String code;
}
