package zubarev.restservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import zubarev.restservice.domain.Product;

import java.util.Optional;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
    Optional<Product> getByCode(String code);
}
