package zubarev.restservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import zubarev.restservice.domain.Sale;

import java.util.UUID;

public interface SaleRepository extends JpaRepository<Sale, UUID> {
}
