package zubarev.restservice.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zubarev.restservice.dto.request.SaleData;
import zubarev.restservice.dto.response.SaleResult;
import zubarev.restservice.service.SaleService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/sales")
@RequiredArgsConstructor
@Slf4j
@Validated
public class SaleController {

    private final SaleService saleService;


    @PostMapping
    public SaleResult addSales(@Valid @NotNull @RequestBody SaleData sales) {
        return saleService.addSales(sales);
    }
}
