package zubarev.restservice.mapper;

import org.springframework.stereotype.Component;
import zubarev.restservice.domain.Product;
import zubarev.restservice.domain.Sale;
import zubarev.restservice.dto.request.SaleData;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class SaleMapper {

    public Sale convert(SaleData saleData, List<Product> products) {
        Sale sale = new Sale();
        sale.setCustomer(saleData.getCustomer());
        sale.setSeller(saleData.getSeller());
        sale.setProducts(products);

        return sale;
    }

}
