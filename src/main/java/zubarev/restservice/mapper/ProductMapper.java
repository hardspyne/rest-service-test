package zubarev.restservice.mapper;

import org.springframework.stereotype.Component;
import zubarev.restservice.domain.Product;
import zubarev.restservice.dto.request.ProductDto;

@Component
public class ProductMapper {


    public Product convert(ProductDto dto) {
        Product product = new Product();
        product.setName(dto.getName());
        product.setCode(dto.getCode());
        return product;
    }
}
