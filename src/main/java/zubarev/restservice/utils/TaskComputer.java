package zubarev.restservice.utils;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.function.Function;

public class TaskComputer<K, V> {

    private final Map<K, Future<V>> cache = new ConcurrentHashMap<>();

    public Future<V> compute(K k, Function<K, V> f) {
        return cache.computeIfAbsent(k, (K key) -> CompletableFuture.supplyAsync(() -> f.apply(k)));
    }


}
