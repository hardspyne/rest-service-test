package zubarev.restservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import zubarev.restservice.dao.ProductRepository;
import zubarev.restservice.dao.SaleRepository;
import zubarev.restservice.domain.Product;
import zubarev.restservice.domain.Sale;
import zubarev.restservice.dto.request.SaleData;
import zubarev.restservice.dto.response.SaleResult;
import zubarev.restservice.mapper.ProductMapper;
import zubarev.restservice.mapper.SaleMapper;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class SaleService {
    private final ProductMapper productMapper;
    private final SaleMapper saleMapper;
    private final ProductRepository productRepository;
    private final SaleRepository saleRepository;

    /**
     * Добавление новой продажи продуктов.
     */
    @Transactional
    public SaleResult addSales(SaleData sale) {
        List<Product> products = sale
                .getProducts()
                .stream()
                .map(productMapper::convert)
                .map(this::getOrSaveNewProduct)
                .collect(Collectors.toList());
        Sale newSale = saleMapper.convert(sale, products);
        newSale = saleRepository.save(newSale);
        return new SaleResult(newSale.getId());
    }

    private Product getOrSaveNewProduct(Product product) {
        return productRepository.getByCode(product.getCode())
                .orElseGet(() -> productRepository.save(product));
    }
}
