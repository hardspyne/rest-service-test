package zubarev.restservice.domain;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@EqualsAndHashCode(of = {"id"})
public abstract class AbstractEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private UUID id;


    @Column(name = "created_time")
    private Date createdTime;


    @PrePersist
    public void setCreatedTime() {
        this.createdTime = new Date();
    }
}
