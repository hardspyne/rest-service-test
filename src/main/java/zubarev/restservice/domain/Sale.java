package zubarev.restservice.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sale")
@Getter
@Setter
public class Sale extends AbstractEntity {

    @Column(name = "seller")
    private String seller;

    @Column(name = "customer")
    private String customer;


    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "product_sales",
            joinColumns = { @JoinColumn(name = "sale_id") },
            inverseJoinColumns = { @JoinColumn(name = "product_id") }
    )
    private List<Product> products;
}
