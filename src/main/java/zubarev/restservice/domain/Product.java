package zubarev.restservice.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "product")
@Getter
@Setter
public class Product extends AbstractEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;
}
